package br.ucsal.edu.bes20182.ed;

import java.util.Scanner;

public class Menu {
	static Scanner sc = new Scanner(System.in);
	public static int limite = 0;

	public static void inputVetor() {

		System.out.println("Informe o n�mero total de funcion�rios: ");
		limite = sc.nextInt();
		for (int i = 0; i < limite; i++) {
			Funcionario.todos[i] = new Funcionario();
			System.out.println("Informe o cpf: ");
			Funcionario.todos[i].cpf = sc.nextLong();
			System.out.println("Informe o Nome: ");
			Funcionario.todos[i].nome = sc.next();
			System.out.println("Informe a profiss�o: ");
			Funcionario.todos[i].profissao = sc.next();
			TreeByName.inserir(i);
			TreeByCpf.inserir(i);
		}

	}

	public static void principal() {
		int esc = 0;
		Long cpf;
		System.out.println(
				"Escolha uma op��o para come�ar:\n (1)Busca Por CPF\n (2)Imprimir Por Cpf \n (3)Remover Por Cpf ");
		esc = sc.nextInt();
		switch (esc) {
		case 1:
			System.out.println("Informe o numero de cpf: ");
			cpf = sc.nextLong();
			TreeByCpf.buscar(cpf);
			Menu.principal();
			break;
		case 2:
			No auxCpf = TreeByCpf.raizCpf;
			System.out.println("InOrder");
			TreeByCpf.porCpf.inOrder(auxCpf);
			System.out.println();
			Menu.principal();
			break;
		case 3:
			System.out.println("Informe o numero de cpf: ");
			cpf = sc.nextLong();
			TreeByCpf.porCpf.remover(cpf);
			Menu.principal();
			break;
		default: 
			break;
		}
	}
}

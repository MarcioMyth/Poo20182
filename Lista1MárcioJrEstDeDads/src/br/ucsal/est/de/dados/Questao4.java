package br.ucsal.est.de.dados;

import java.util.Scanner;

public class Questao4 {

	public static void main(String[] args) {
		double pm = 0;
		double tc = 0;
        double pe1 = 0, pe2 = 0, pe3 = 0,pe4 = 0, pe5= 0;
        // Entrada de Dados
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe a Popula��o Mundial Atual: ");
        pm = scanner.nextDouble();
        System.out.println("Informe a Taxa de Crescimento Anual: ");
        tc = scanner.nextDouble();
        //Processamento
        pe1 = pm + tc;
        pe2 = pm + tc *2;
        pe3 = pm + tc *3;
        pe4 = pm + tc *4;
        pe5 = pm + tc *5;
        //Saida
        System.out.println("Popuila��o Estimada ");
        System.out.println("Em Um Ano: "+ pe1);
        System.out.println("Em Dois Anos: "+pe2);
        System.out.println("Em Tres Anos: "+pe3);
        System.out.println("Em Quatro Anos: "+pe4);
        System.out.println("Em Cinco Anos: "+ pe5);
	}

}

package br.ucsal.est.de.dados;

import java.util.Scanner;

public class Questao2 {

	public static void main(String[] args) {
		String nome = "";
		double peso = 0;
		double altura = 0;
		double imc = 0;
		String cond = "";
       //Entrada de Dados
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe seu nome: ");
		nome = scanner.nextLine();
		System.out.println("Informe seu peso: ");
		peso = scanner.nextDouble();
		System.out.println("Informe sua altura: ");
		altura = scanner.nextDouble();
		//Processamento
		imc = peso / Math.pow(altura, 2);
		if (imc <= 18.5){
			cond = "Abaixo do Peso";
		}else if (imc > 18.5 && imc <=25){
			cond = "Peso Normal";
		}else if (imc >25 && imc <= 30){
			cond = "Acima do Peso";	
		}else if (imc > 30){
			cond = "Obeso";
		}
		//Saida
		System.out.println("Sua Classifica��o � : "+cond);
		
	}

}

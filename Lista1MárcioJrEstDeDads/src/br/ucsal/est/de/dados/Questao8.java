package br.ucsal.est.de.dados;

import java.util.Scanner;

public class Questao8 {

	public static void main(String[] args) {
		String [] nome = new String [11];
		double [] sal = new double [11];
		double [] aliq = new double [11];
		//Entrada de Dados
		Scanner scanner = new Scanner(System.in);
		for(int i = 1; i <=10;i++) {
			System.out.println("Informe o nome do funcion�rio "+i);
			nome [i] = scanner.nextLine();					
		}
		for(int i = 1;i <=10;i++) {
			System.out.println("Informe sal�rio Bruto do funcion�rio "+i);
			sal [i] = scanner.nextDouble();
		}
		
		//Processamento
		for (int i = 1; i <=10 ; i++) {
			if (sal [i] < 600) {
		    aliq [i] = sal [i];
			}else if(sal [i] >= 600 && sal [i] < 1500 ) {
				aliq [i] = (10 * sal [i]) /100;
			}else if(sal [i] >= 1500) {
				aliq [i] = (15 * sal[i])/100;
			}else {
				System.out.println("ERRO");
			}
		}
		//Sa�da
		for (int i =1; i <=10 ; i++) {
			System.out.println("A aliquota do imposto de renda de: ");
			System.out.println(nome[i]);
			System.out.println("� igual a R$"+aliq[i]);
			System.out.println("");
		}
	}

}

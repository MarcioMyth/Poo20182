package br.ucsal.bes20182.poo.bank.domain;

import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;

public class Cliente {

	private long salario;
	private String rg;
	private Integer conta;
	private EstadoDaConta estado;
	private Double saldo;
	private String nome;
	private String cpf;
	private String telefone;
	private String cep;

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Cliente(String name, String cpf, String tel, String cep, long salario, String rg, Integer account,
			EstadoDaConta estado, Double balance) {
		super();
		this.nome = name;
		this.cpf = cpf;
		this.telefone = tel;
		this.cep = cep;
		this.salario = salario;
		this.rg = rg;
		this.conta = account;
		this.estado = estado;
		this.saldo = balance;
	}

	public EstadoDaConta getEstado() {
		return estado;
	}

	public void setEstado(EstadoDaConta estado) {
		this.estado = estado;
	}

	public Integer getConta() {
		return conta;
	}

	public void setConta(Integer conta) {
		this.conta = conta;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public long getRendaSalarial() {
		return salario;
	}

	public void setRendaSalarial(long rendaSalarial) {
		this.salario = rendaSalarial;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

}

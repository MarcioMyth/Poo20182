package br.ucsal.bes20182.poo.bank.tui;

import br.ucsal.bes20182.poo.bank.business.ClienteBO;
import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;

public class Cadastrar {
	public static void cadastrarCliente() {
		long rendaSalarial = 0;

		System.out.println("Informe o nome: ");
		String nome = MenuTui.sc.nextLine();


		System.out.println("Informe o telefone: ");
		String telefone = MenuTui.sc.nextLine();


		System.out.println("Informe o cpf: ");
		String cpf = MenuTui.sc.nextLine();


		System.out.println("Informe o cep: ");
		String cep = MenuTui.sc.nextLine();


		MenuTui.x = false;
		while(MenuTui.x == false) {
			try {
				System.out.println("Informe sua renda salarial: ");
				rendaSalarial = MenuTui.sc.nextLong();
				MenuTui.x = true;
			}
			catch(Exception e) {
				System.out.println("Informe somente n�meros inteiros.\n");
				MenuTui.sc.next();
			}
		}

		System.out.println("Informe o rg: ");
		String rg = MenuTui.sc.next();

		System.out.println("\n Conta ativa!");
		EstadoDaConta estado = EstadoDaConta.ATIVA;

		MenuTui.sc.nextLine();

		Double saldo = (double) 0;

		Cliente cliente = new Cliente(nome, telefone, cpf, cep, rendaSalarial, rg, MenuTui.conta, estado, saldo);
		MenuTui.conta++;
		if(ClienteBO.validarCliente(cliente) == null)
			System.out.println("Cliente adicionado na lista!");
		else {
			System.out.println(ClienteBO.validarCliente(cliente));
		}

		MenuTui.menu();
	}
}

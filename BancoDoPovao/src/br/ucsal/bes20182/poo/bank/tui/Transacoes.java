package br.ucsal.bes20182.poo.bank.tui;

import java.util.List;

import br.ucsal.bes20182.poo.bank.domain.Cliente;
import br.ucsal.bes20182.poo.bank.enums.EstadoDaConta;
import br.ucsal.bes20182.poo.bank.persistance.ClienteDAO;

public class Transacoes {
	public static void transacoes() {
		
		List<Cliente> clientes = ClienteDAO.todosClientes();
		if (clientes.size() == 0) {
			System.out.println("N�o h� clientes!");
			MenuTui.menu();
		}
		System.out.println("(1) CONSULTAR SALDO (2) DEP�SITO (3) SAQUE (4) MENU PRINCIPAL");
		int escolha = MenuTui.sc.nextInt();

		switch (escolha) {
		case 1:
			System.out.println("CONSULTAR SALDO");
			System.out.println("Informe sua conta: ");
			int informeConta =  MenuTui.sc.nextInt();

			for (Cliente cliente : clientes) {
				if (informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVA) {
					System.out.println("Seu saldo �: " + cliente.getSaldo());

					MenuTui.menu();
				}
			}

			System.out.println("Conta inv�lida!");
			MenuTui.menu();

		case 2:
			System.out.println("DEP�SITO");
			System.out.println("Informe sua conta: ");
			informeConta =  MenuTui.sc.nextInt();

			for (Cliente cliente : clientes) {
				if (informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVA) {
					System.out.println("Seu saldo �: " + cliente.getSaldo());
					System.out.println("Insira o valor de dep�sito: ");
					double depositarDinheiro =  MenuTui.sc.nextDouble();
					cliente.setSaldo(depositarDinheiro + cliente.getSaldo());

					MenuTui.menu();
				}
			}

			System.out.println("Conta inv�lida!");
			MenuTui.menu();

		case 3:
			System.out.println("SACAR");
			System.out.println("Informe sua conta: ");
			informeConta =  MenuTui.sc.nextInt();

			for (Cliente cliente : clientes) {
				if (informeConta == cliente.getConta() && cliente.getEstado() == EstadoDaConta.ATIVA) {
					System.out.println("Seu saldo �: " + cliente.getSaldo());
					System.out.println("Insira o valor que deseja sacar: ");
					double sacarDinheiro =  MenuTui.sc.nextDouble();
					if (cliente.getSaldo() >= sacarDinheiro)
						cliente.setSaldo(cliente.getSaldo() - sacarDinheiro);
					else
						System.out.println("Voc� n�o possu� tanto dinheiro! ");
					MenuTui.menu();
				}

			}

			System.out.println("Conta inv�lida!");
			MenuTui.menu();
		}
	}
}

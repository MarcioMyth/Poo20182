package br.ucsal.bes.poo20182.listao;

import java.util.Scanner;

public class Questaoquatro {

	public static void main(String[] args) {
		System.out.println("@@@@@@@@@@@@@@@ Calculador de Area da Circunferencia @@@@@@@@@@@@@@@@@@@@");
        //Cria��o de Vari�veis
		double area = 0;
		double raio= 0;
		//Entrada de Dados
        Scanner scanner = new Scanner(System.in);
        System.out.println("Informe o raio: ");
        raio = scanner.nextFloat();		
        //Processamento
        area = 3.14*Math.pow(raio, 2);
        //Saida
        System.out.println("A area dessa circunferencia � igual a: "+ area + " metros quadrados!");
	}

}

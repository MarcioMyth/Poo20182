package br.ucsal.bes.poo20182.listao;

import java.util.Scanner;

public class Questaotreze {

	public static void main(String[] args) {
		double a = 0;
		double b = 0;
		double aux = 0;
		//Entrada de Dados
		System.out.println("Informe o valor de A:");
        Scanner scanner = new Scanner(System.in);
        a = scanner.nextDouble();
        System.out.println("Informe o valor de B: ");
        b = scanner.nextDouble();
        //Processamento
        aux = a;
        a = b;
        //Saida
        System.out.println("Valores Trocados: \n O valor de A agora vale "+ a+ " e o valor de B: "+ aux);
        
	}

}

package br.ucsal.bes.poo20182.listao;

import java.util.Scanner;

public class Questaodezesseis {

	public static void main(String[] args) {
		int valor = 0;
		double quad = 0;
		double cubo = 0;
		//Entrada de Dados
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe um valor inteiro: ");
		valor = scanner.nextInt();
		//Processamento
		quad = Math.pow(valor, 2);
		cubo = Math.pow(valor, 4);
		//Saida
		System.out.println("O quadrado desse Valor �: "+ quad);
		System.out.println("O cubo desse valor �: "+ cubo);
		

	}

}

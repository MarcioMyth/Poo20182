package br.ucsal.bes.poo20182.listao;

import java.util.Scanner;

public class Questaoquinze {

	public static void main(String[] args) {
		double comprimento = 0;
		double altura = 0;
		double largura = 0;
		double volume = 0;
		//Entrada de Dados
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe o valor do Comprimento: ");
		comprimento = scanner.nextDouble();
		System.out.println("Informe o valor da altura: ");
		altura = scanner.nextDouble();
		System.out.println("Informe o valor da largura: ");
		largura = scanner.nextDouble();
		//Processamento
		volume = comprimento *largura*altura;
		//Saida de Dados
		System.out.println("O volume de uma caixa retangular com esses valores � igual a: "+volume + "Litros");
	}

}

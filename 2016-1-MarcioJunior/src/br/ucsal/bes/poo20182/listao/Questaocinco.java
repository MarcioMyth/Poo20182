package br.ucsal.bes.poo20182.listao;

import java.util.Scanner;

public class Questaocinco {

	public static void main(String[] args) {
		System.out.println("Calculador de Area do Triangulo");
		//Declara��o de Var�veis
		double base = 0;
		double altura = 0;
		double area = 0;
		//Entrada de Dados
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe a valor da base do trinagulo:");
		base = scanner.nextDouble();
		System.out.println("Informe a altura do triangulo:");
		altura = scanner.nextDouble();
		//Processamento
		area = base*altura/2;
		//Saida
		System.out.println("A area desse triangulo �: "+area+"m2");

	}

}

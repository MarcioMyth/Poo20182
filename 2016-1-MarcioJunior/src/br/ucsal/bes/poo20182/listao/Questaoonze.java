package br.ucsal.bes.poo20182.listao;

import java.util.Scanner;

public class Questaoonze {

	public static void main(String[] args) {
		double volume = 0;
		double raio = 0;
		double altura = 0;
		//Entrada de Dados
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe o raio do recipiente: ");
		raio = scanner.nextDouble();
		System.out.println("Informe a altura do recipiente: ");
        altura = scanner.nextDouble();
        //Processamento
        volume = 3.14*Math.pow(raio, 2)*altura;
        // Saida
        System.out.println("O volume do recipiente �: "+volume + "litros");
	}

}

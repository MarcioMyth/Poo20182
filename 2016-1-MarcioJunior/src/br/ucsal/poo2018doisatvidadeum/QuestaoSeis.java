package br.ucsal.poo2018doisatvidadeum;

import java.util.Scanner;

public class QuestaoSeis {

	public static void main(String[] args) {
		
		//Declara��o de vetor e Vari�vel
		int [] n1 = new int [10];
		int [] n2 = new int [10];
		int [] ntot =new int [10];
		int valor = 0;
		//Entrada de dados
		Scanner scanner = new Scanner(System.in);
		for(int i = 0; i < 10;i++) {//Preenchimento Vetor 1
			System.out.println("Vetor 1 informe o valor da posi��o " + i+ ":");
			valor = scanner.nextInt();
			n1 [i]= valor;
		}
		for(int i = 0;i <10;i++) { //Preenchimento Vetor 2
			System.out.println("Vetor 2 informe o valor da posi��o " + i + ":");
			valor = scanner.nextInt();
			n2 [i] = valor;
		}
		//Processamento
		for(int i = 0; i <10;i++) { //Soma dos vetores 1 + 2
			ntot [i] = n1 [i] + n2[i];
		}
	
		//Sa�da
		System.out.println("A soma entre os Vetores 1 + 2 �: ");
	for(int cont = 0; cont < 10;cont ++) {
		System.out.print( ntot[cont]+ " ");
	}
		
	}       

}

package br.ucsal.poo2018doisatvidadeum;

import java.util.Scanner;

public class QuestaoCinco {

	public static void main(String[] args) {
		// Declara��o de Vari�veis
		int fat = 1;
		int valor = 0;
		
		//Entrada de Dados
		Scanner scanner = new Scanner(System.in);
		System.out.println("Informe um valor: ");
		valor = scanner.nextInt();
		
		//Processamento
		for(int i = 2; i <= valor;i++) {
			fat = fat * i;
		}
		
		//Sa�da
		System.out.println("O fatorial de "+ valor + " � igual a "+ fat);

	}

}

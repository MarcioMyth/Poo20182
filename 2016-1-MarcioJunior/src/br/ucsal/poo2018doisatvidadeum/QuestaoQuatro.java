package br.ucsal.poo2018doisatvidadeum;

import java.util.Scanner;

public class QuestaoQuatro {

	public static void main(String[] args) {
		//Declara��o de Vari�vel
		String letra = "";
	    String tipo = "";
	    
	    //Entrada de Dados
	    Scanner scanner = new Scanner(System.in);
        System.out.println("Informe uma letra do alfabeto: ");
        letra = scanner.nextLine();
        
        //Processamento
        if (letra.equals("a")||letra.equals("A")) {
        	tipo = "Vogal";        	
        }else if(letra.equals("e")|| letra.equals("E")) {
        	tipo = "Vogal";
        }else if(letra.equals("i")||letra.equals("I")) {
        	tipo = "Vogal";
        }else if(letra.equals("o")|| letra.equals("O")) {
        	tipo = "Vogal";
        }else if(letra.equals("u")||letra.equals("U")) {
        	tipo = "Vogal";
        }else {
        	tipo = "Consoante";
        }
        
        //Sa�da
        System.out.println("A letra � uma "+ tipo);
	}   

}

package br.ucsal.bes20182.ed.lista3;

public class Questao05 {
	// ALUNO : M�rcio Brand�o J�nior
	static Elemento primeiro = null;

	public void alterarNumeros(Integer numero1, Integer numero2) {
		if (primeiro == null) {
			System.out.println("Lista vazia!");
		} else {
			Elemento aux1 = primeiro;
			Elemento aux2 = primeiro;

			while (aux1.prox != null && aux1.valor != numero1) {
				aux1 = aux1.prox;
			}

			while (aux2.prox != null && aux2.valor != numero2) {
				aux2 = aux2.prox;
			}

			if (aux1.valor != numero1 && aux2.valor != numero2) {
				System.out.println("Os n�meros a seguir n�o foram encontrados: " + numero1 + numero2);
			} else if (aux1.valor != numero1) {
				System.out.println(" O N�mero a seguir n�o foi encontrado: " + numero1);
			} else if (aux2.valor != numero2) {
				System.out.println("O n�mero a seguir n�o foi encontrado: " + numero2);
			} else {
				int aux = aux1.valor;
				aux1.valor = aux2.valor;
				aux2.valor = aux;
				System.out.printf("Os n�meros a seguir trocaram de posi��o na lista" + numero1 + " " + numero2);
			}
		}
	}

	class Elemento {
		public Integer valor;
		public Elemento prox;
	}
}

package br.ucsal.bes20182.ed.lista3;

public class Questao01 {
	// ALUNO : M�rcio Brand�o J�nior

	public static void main(String[] args) {
		Questao01.mostrar();
		Questao01.inserir(50);
		Questao01.inserir(25);
		Questao01.inserir(10);
		Questao01.mostrar();
		Questao01.retornarReferencia(25);
	}

	static Elemento primeiro = null;

	public static void inserir(Integer valorEntrada) {
		Elemento novo = new Elemento();
		novo.valor = valorEntrada;

		if (primeiro == null) {
			primeiro = novo;
		} else {
			Elemento aux = primeiro;

			while (aux.prox != null) {
				aux = aux.prox;
			}

			aux.prox = novo;
		}
	}

	public static void mostrar() {
		System.out.println("################## LISTA ####################");
		if (primeiro == null) {
			System.out.println("Lista vazia!");
		} else {
			Elemento aux = primeiro;

			while (aux != null) {
				System.out.print(aux.valor + " ");
				aux = aux.prox;
			}
			System.out.println();
		}
	}

	public static void retornarReferencia(Integer number) {
		Elemento aux = primeiro;

		if (aux == null) {
			System.out.println("Lista vazia!");
		} else {
			while (aux.prox != null && aux.valor != number) {
				aux = aux.prox;
			}

			if (aux.valor != number) {
				System.out.println("N�o consta na lista.");
			} else {
				System.out.println(aux);
			}
		}
	}
}

class Elemento {
	public Integer valor;
	public Elemento prox;
}

package br.ucsal.bes20182.poo.agenda.business;

import java.util.List;

import br.ucsal.bes20182.poo.agenda.domain.Contato;
import br.ucsal.bes20182.poo.agenda.persistence.ContatoDAO;

public class ContatoBO {

	public ContatoDAO contatoDAO = new ContatoDAO();

	public String incluir(Contato contInclui) {
		String erro = validar(contInclui);
        if (erro != null) {
        	return erro;
        }
        return contatoDAO.incluir(contInclui);
	}

	public List<Contato> obterTodos() {
		return contatoDAO.obterTodos();
	}

	private String validar(Contato contatoValid) {
		if (contatoValid.getNome().trim().isEmpty()) {
			return "ERRO, NOME N�O INFORMADO";
		}
		if (contatoValid.getTelefone().trim().isEmpty()) {
			return "ERRO, TELEFONE N�O INFORMADO";

		}
		return null;
	}
}

package br.ucsal.bes20182.poo.agenda.tui;

import java.util.List;
import java.util.Scanner;

import br.ucsal.bes20182.poo.agenda.business.ContatoBO;
import br.ucsal.bes20182.poo.agenda.domain.Contato;

public class ContatoTui {
	public Scanner scanner = new Scanner(System.in);
	public ContatoBO contatoBO = new ContatoBO();

	private String obterText(String mensagem) {
		System.out.println(mensagem);
		return scanner.nextLine();

	}

	private Integer obterInt(String mensagem) {
		System.out.println(mensagem);
		Integer numero = scanner.nextInt();
		scanner.nextLine();
		return numero;
	}

	public void incluir() {
		System.out.println("INCLUINDO NOVO CONTATO");
		String nome = obterText("Informe o nome do contato: ");
		String telefone = obterText("Informe o telefone do contato: ");
		Integer anoNascimento = obterInt("Informe o ano de nascimento do contato: ");

		Contato contato = new Contato(nome, telefone, anoNascimento);

		String erro = contatoBO.incluir(contato);
		if (erro != null) {
			System.out.println("N�o foi poss�vel criar o contato");
			System.out.println(erro);
		}
	}

	public void listar() {

		System.out.println("CONTATOS DA AGENDA");
		List<Contato> contatos = contatoBO.obterTodos();

		for (int i = 0; i < contatos.size(); i++) {
			System.out.println("Contato " + (i + 1));
			System.out.println("\t" + contatos.get(i).getNome());
			System.out.println("\t Telefone: " + contatos.get(i).getTelefone());
			System.out.println("\t Ano de Nascimento: " + contatos.get(i).getAnoNas());
		}
	}

	public void buscar() {
		System.out.println("BUSCANDO NA AGENDA");
		List<Contato> contatos = contatoBO.obterTodos();

		String name = obterText("Informe o primeiro nome contato: ");
		for (int i = 0; i < contatos.size(); i++) {
			if (contatos.get(i).getNome().equalsIgnoreCase(name)) {
				System.out.println(contatos.get(i).toString());
			} else {
				System.out.println("Contato n�o existe!");
			}

		}

	}

}

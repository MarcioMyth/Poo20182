package br.ucsal.bes20182.poo.agenda.tui;

import java.util.Scanner;

public class AgendaTui {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		ContatoTui contatoTui = new ContatoTui();

		Object op;
		do { // MENU
			// Entrada de Dados do Menu
			System.out.println(
					"O que deseja? \n(1)Incluir Contato \n(2)Excluir Contato\n(3)Listar Contatos\n(4)Pesquisar Contato ");
			int esc = sc.nextInt();

			switch (esc) {
			case 1:// Adicionar novo Contato
				contatoTui.incluir();
				break;

			case 2:// Excluir um Contato

				break;
			case 3:// Listar Contatos	
				contatoTui.listar();
				break;

			case 4:// Buscar Contato
                contatoTui.buscar();
				break;
			default:
				// Saida de Dados
				System.out.println("Op��o Inv�lida, Tente novamente");
			}
			// Entrada de Dados
			System.out.println("Deseja Voltar ao Menu? ");

			op = sc.next();

		} while (op.equals("s") || op.equals("S"));

	}

}

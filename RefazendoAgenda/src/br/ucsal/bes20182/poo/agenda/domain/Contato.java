package br.ucsal.bes20182.poo.agenda.domain;

public class Contato {
	private String nome = " ";
	private String tel = null;
	private Integer anoN = null;

	public Contato(String name, String telefone, Integer anoNascimento) {
		this.nome = name;
		this.tel = telefone;
		this.anoN = anoNascimento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nom) {
		this.nome = nom;
	}

	public String getTelefone() {
		return tel;
	}

	public void setTelfone(String tele) {
		this.tel = tele;
	}

	public Integer getAnoNas() {
		return anoN;
	}

	public void setAnoNas(Integer ano) {
		this.anoN = ano;
	}
}
